package com.example.steffen.movs_eventbus;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
    }


    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @OnClick(R.id.btA)
    public void ButtonAClick() {
        Toast.makeText(this, "Button A Click", Toast.LENGTH_SHORT).show();
        EventBus.getDefault().post(new MyEventA(2,3));
    }


    @OnClick(R.id.btB)
    public void ButtonBClick() {
        EventBus.getDefault().post(new MyEventB(2,3));
    }


    // This method will be called when a SomeOtherEvent is posted
    @Subscribe
    public void handleSomeEvent(MyEventA event) {
        Toast.makeText(this, "Event A Received: " + event.getC(), Toast.LENGTH_SHORT).show();
        Log.wtf("********", "A1");
    }

    @Subscribe
    public void handleSomeEvent2(MyEventA event) {
        Toast.makeText(this, "Event A2 Received: " + event.getC(), Toast.LENGTH_SHORT).show();
        Log.wtf("********", "A2");
    }


    @Subscribe
    public void handleSomethingElse(MyEventB event) {
        Toast.makeText(this, "Event B Received: " + event.getC(), Toast.LENGTH_SHORT).show();
    }


}
