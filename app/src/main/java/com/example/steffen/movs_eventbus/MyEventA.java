package com.example.steffen.movs_eventbus;

/**
 * Created by Steffen on 10.03.2017.
 */

public class MyEventA {

    int a;
    int b;
    int c;

    public MyEventA(int a, int b) {
        this.a = a;
        this.b = b;
        this.c = a + b;
    }

    public int getC() {
        return c;
    }
}
